#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: diaspora_federation-json_schema 0.2.6 ruby lib

Gem::Specification.new do |s|
  s.name = "diaspora_federation-json_schema".freeze
  s.version = "0.2.6"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Benjamin Neff".freeze, "cmrd Senya".freeze]
  s.date = "2019-04-28"
  s.description = "This gem provides JSON schemas (currently one schema) for validating JSON serialized federation objects.".freeze
  s.email = ["benjamin@coding4.coffee".freeze, "senya@riseup.net".freeze]
  s.files = ["lib/diaspora_federation/schemas.rb".freeze, "lib/diaspora_federation/schemas/federation_entities.json".freeze]
  s.homepage = "https://github.com/diaspora/diaspora_federation".freeze
  s.licenses = ["AGPL-3.0".freeze]
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "diaspora* federation json schemas".freeze
end
